package com.example.demo.pojo;


public class User {

    private String id1;

    private String id2;

    private String userID;

    public User(String id1, String id2, String userID) {
        this.id1 = id1;
        this.id2 = id2;
        this.userID = userID;
    }

    public User() {
    }

    public String getId1() {
        return id1;
    }

    public void setId1(String id1) {
        this.id1 = id1;
    }

    public String getId2() {
        return id2;
    }

    public void setId2(String id2) {
        this.id2 = id2;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
