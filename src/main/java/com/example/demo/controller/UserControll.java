package com.example.demo.controller;

import com.alibaba.fastjson.JSON;
import com.example.demo.pojo.User;
import com.example.demo.server.UserServerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserControll {

    @Autowired
    private UserServerImpl userServer;

    @PostMapping("/getuser")
    public String getUser(String id1, String id2)
    {
        User user = userServer.getUser(id1, id2);
        String result = JSON.toJSONString(user);
        return result;
    }
}
