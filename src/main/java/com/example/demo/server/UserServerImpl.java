package com.example.demo.server;

import com.example.demo.dao.UserMapper;
import com.example.demo.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.UUID;


@Service("userServerImpl")
public class UserServerImpl implements UserServer {

    @Autowired
    private UserMapper userMapper;


    @Override
    public User getUser(String id1, String id2) {
        User user = userMapper.getUser(id1, id2);
        if (user == null || StringUtils.isEmpty(user.getUserID())){
            User newUser = new User(id1,id2, UUID.randomUUID().toString());
            userMapper.saveUser(newUser);
            user = newUser;
        }
        return user;
    }
}
