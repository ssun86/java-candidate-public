package com.example.demo.server;


import com.example.demo.pojo.User;

public interface UserServer {

    User getUser(String id1, String id2);

}
